package com.example.assignment4;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.design.internal.ViewUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.support.constraint.ConstraintLayout.*;

public class DisplayActivity extends AppCompatActivity {


    private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);


        db = new DbHelper(this);

        TableLayout tableLayout=(TableLayout)findViewById(R.id.tablelayout1);
        LinearLayout linearLayout=new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setShrinkAllColumns(true);

        TableRow rowTile=new TableRow(this);
        rowTile.setGravity(Gravity.CENTER);

        TableRow trow=new TableRow(this);

        TextView textView=new TextView(this);
        textView.setText("User Details....");

        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18);
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(Typeface.SERIF,Typeface.BOLD);
        rowTile.addView(textView);

        TextView t1=new TextView(this);
        TextView t2=new TextView(this);
        TextView t3=new TextView(this);
        TextView t4=new TextView(this);
        TextView t5=new TextView(this);
        TextView t6=new TextView(this);
        TextView t7=new TextView(this);

        t1.setText("id");
        t2.setText("email");
        t3.setText("name");
        t4.setText("faname");
        t5.setText("phnumber");
        t6.setText("date");
        t7.setText("address");

        t1.setPadding(8,0,8,0);
        t2.setPadding(8,0,8,0);
        t3.setPadding(8,0,8,0);
        t4.setPadding(8,0,8,0);
        t5.setPadding(8,0,8,0);
        t6.setPadding(8,0,8,0);
        t7.setPadding(8,0,8,0);

        linearLayout.addView(t1);
        linearLayout.addView(t2);
        linearLayout.addView(t3);
        linearLayout.addView(t4);
        linearLayout.addView(t5);
        linearLayout.addView(t6);
        linearLayout.addView(t7);


        trow.addView(linearLayout);

        tableLayout.addView(rowTile);
        tableLayout.addView(trow);









    }


}
