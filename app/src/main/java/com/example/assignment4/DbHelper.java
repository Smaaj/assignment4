package com.example.assignment4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {


    public static final String TAG = DbHelper.class.getSimpleName();
    public static final String DB_Name = "myapp.db";
    public static final int DB_Version = 1;

    public static final String USER_TABLE = "users";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASS = "password";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_FN = "faname";
    public static final String COLUMN_PHONE = "phnumber";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_ADDRESS = "address";


    /*Create User Table*/

    public static final String CREATE_TABLE_USERS = " CREATE TABLE " + USER_TABLE + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_EMAIL + " TEXT,"
            +COLUMN_PASS + " TEXT,"
            +COLUMN_NAME + " TEXT,"
            +COLUMN_FN + " TEXT,"
            +COLUMN_PHONE + " NUMBERS,"
            +COLUMN_DATE + "NUMBERS,"
            +COLUMN_ADDRESS+ "TEXT);";


    public DbHelper(Context context) {

        super(context, DB_Name, null, DB_Version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + USER_TABLE);
        onCreate(sqLiteDatabase);

    }

    /*storing user details in database*/

    public void addUser(String email,
                        String name, String faname, String phnumber, String date, String address,
                        String password){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_NAME, name);
        values.put(COLUMN_FN, faname);
        values.put(COLUMN_PHONE, phnumber);
        values.put(COLUMN_DATE, date);
        values.put(COLUMN_ADDRESS, address);
        values.put(COLUMN_PASS, password);

        long id = sqLiteDatabase.insert(USER_TABLE, null, values);
        sqLiteDatabase.close();

        Log.d(TAG, "user inserted" + id);
    }
    public boolean getUser(String email, String password) {
        //HashMap <String, String> user = new HashMap <String ,String > ();
        String selectQurey = "select * from " + USER_TABLE + " where " +
                COLUMN_EMAIL + " = " + "'" + email + "'" + " and " + COLUMN_PASS + " = " + "'" + password + "'";

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQurey, null);




        if (cursor.moveToFirst()) {
            do {
                email = cursor.getString(1);

                if (email.equals(email)) {
                    password = cursor.getString(7);
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        return true;
    }
}