package com.example.assignment4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import static android.view.View.*;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener{


        private ImageButton imBtnReg;

        private EditText etPlaNmeReg ,etPlaFNReg , etPhnReg , etEmReg , etDtReg , etPtAddReg, etPass;

        private Button btnPerReg;

        private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        imBtnReg = (ImageButton) findViewById(R.id.imBtnReg);
        etPlaNmeReg = (EditText) findViewById(R.id.etPlaNmeReg);
        etPlaFNReg = (EditText) findViewById(R.id.etPlaFNReg);
        etPhnReg = (EditText) findViewById(R.id.etPhnReg);
        etEmReg = (EditText) findViewById(R.id.etEmReg);
        etDtReg = (EditText) findViewById(R.id.etDtReg);
        etPtAddReg = (EditText) findViewById(R.id.etPtAddReg);
        etPass=(EditText) findViewById(R.id.etPass);
        btnPerReg = (Button) findViewById(R.id.btnPerReg);


        db = new DbHelper(this);

        btnPerReg.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPerReg:
                register();
                break;
            default:
        }
    }
    private void register(){
                String email = etEmReg.getText().toString();
                String name = etPlaNmeReg.getText().toString();
                String faname = etPlaFNReg.getText().toString();
                String phnumber = etPhnReg.getText().toString();
                String date = etDtReg.getText().toString();
                String address = etPtAddReg.getText().toString();
                String password = etPass.getText().toString();

        if(email.isEmpty() || name.isEmpty() || faname.isEmpty() || phnumber.isEmpty() || date.isEmpty() || address.isEmpty() || password.isEmpty()){
            displayToast("please fill all fields");
        }else {
            db.addUser(email, name, faname, phnumber, date, address, password);
            displayToast("User Registered");
            finish();
        }
    }
    private void displayToast(String message){
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
    }
}