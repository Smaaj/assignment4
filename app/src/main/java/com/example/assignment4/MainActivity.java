package com.example.assignment4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnWel, btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnWel = (Button) findViewById(R.id.btnWel);
        btnSignup = (Button) findViewById(R.id.btnSignup);

        btnWel.setOnClickListener(this);
        btnSignup.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignup:
                startActivity(new Intent(MainActivity.this, RegistrationActivity.class));

                break;
            case R.id.btnWel:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                break;
            default:

        }
    }
}