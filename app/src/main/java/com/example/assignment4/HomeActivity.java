package com.example.assignment4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

        private Button btnPerHo , btnSetting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnPerHo = (Button)findViewById(R.id.btnPerHo);
        btnSetting = (Button)findViewById(R.id.btnSetting);

        btnPerHo.setOnClickListener(this);
        btnSetting.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPerHo:
                startActivity(new Intent(HomeActivity.this,DisplayActivity.class));
                finish();
                break;

            case R.id.btnSetting:
                startActivity(new Intent(HomeActivity.this,SettingActivity.class));
                finish();
                break;
                default:


        }
    }
}
