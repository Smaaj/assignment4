package com.example.assignment4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{


    private Button btnLogAct;
    private EditText etPlem, etPass;
    private DbHelper db;
    private Session session;
    private TextView tvForget;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        db = new DbHelper(this);
        session = new Session(this);
        btnLogAct = (Button)findViewById(R.id.btnLogAct);
        tvForget = (TextView) findViewById(R.id.tvForget);
        etPlem = (EditText)findViewById(R.id.etPlem);
        etPass = (EditText)findViewById(R.id.etPass);
        btnLogAct.setOnClickListener(this);
        tvForget.setOnClickListener(this);

        if(session.loggedin()){
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
            finish();
        }

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btnLogAct:
                login();
                break;
            case R.id.tvForget:
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
                break;
            default:

        }
    }

    private void login(){
        String email = etPlem.getText().toString();
        String password = etPass.getText().toString();

        if(db.getUser(email,password)){
            session.setLoggedin(true);
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }else{
            Toast.makeText(getApplicationContext(), "Wrong email/password", Toast.LENGTH_SHORT).show();
        }

    }
}
